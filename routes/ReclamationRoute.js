const express = require('express');
const router =express.Router();
const Reclamation = require('../models/Reclamation');
const U = require('../models/User');

var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');

function verifyToken(req, res, next) {
    let payload;
    if(req.query.token === 'null') {
        return res.status(401).send('Unauthorized request')
    }
    try{payload = jwt.verify(req.query.token,   'tawfik');} catch (e) {
        return res.status(400).send('Invalid User');
    }
    if(!payload) {
        return res.status(401).send('Unauthorized request');
    }

    decoded=jwt.decode(req.query.token, {complete: true});
    req.id = decoded.payload.id;

    next()
}

router.post('/Reclamation/add', verifyToken, async (req, res) => {
    try {
        const user = await U.findOne({_id: req.id});
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'iottreetronixt@gmail.com',
                pass: 'IOT26116986'
            }
        });

        var mailOptions = {
            from: 'iottreetronixt@gmail.com',
            to: 'tawfikmili3@gmail.com' ,
            subject: 'Reclamation From  : ' + user.username,
            text: 'Subject : '  + req.body.subject +  '\n Message :' + req.body.message,
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
               
            }
        });
    let reclamation = new Reclamation({
        userId: req.id,
        message: req.body.message,
        subject :req.body.subject,

    });


        reclamation = await reclamation.save();
       
        res.json({status: "ok", message: 'reclamation add to data base'});
    } catch (err) {
        res.json({status:'err',message: err.message});
    }

});

router.get('/all',verifyToken,async (req,res) =>
{

    try{
        const s = await Reclamation.find( { userId : req.id}
        );
        
        res.json(s) ;

    }catch (err) {
        res.json({ message:err.message });

    }

}) ;




router.delete('/Reclamation/delete/:id',(req, res) => {
   
    Reclamation.findByIdAndRemove(req.params.id)
        .then(reclamation => {
            if(!reclamation) {
                return res.status(404).send({
                    message: "sensor not found with code " + req.params.id
                });
            }
        })
} );
module.exports=router;
