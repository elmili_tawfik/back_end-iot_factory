const kafka = require('kafka-node');
try {
    Consumer = kafka.Consumer,
        client = new kafka.KafkaClient({kafkaHost: '193.95.76.211:9092'}),
        consumer = new Consumer(client, [{topic: 'AS.Treetronix.v1', partition: 0}],
            {autoCommit: true}
        );
    console.log('kafka') ;
    consumer.on('message', function (msg) {

        const obj = JSON.parse(msg.value);
        console.log(obj) ;

    });
    consumer.on('error', function (err) {
        console.log('error', err);;
    });
} catch (e) {
    console.log(e);
}
